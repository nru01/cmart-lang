#include <stdio.h>



#define INTEGER_BIT	1
#define SIGNED_BIT	2
#define CHARACTER_BIT	4
#define POINTER_BIT	8
#define VAR_CONTR_BIT	16
#define GET_SIZE_SHIFT	5
#define POINTER_SIZE	8
#define NOT_POINTER	-1

#define UINT_NAND	6
#define UINT_OR		1
#define SINT_NAND	4
#define SINT_OR		3
#define FLOAT_NAND	5
#define FLOAT_OR	2
#define CHAR_NAND	2
#define CHAR_OR		5
#define TYPE_ERR_NAND	3

// STRUCTS
struct ValType {
	short type;
};
struct ValType U8_int = {.type=(short)((8<<GET_SIZE_SHIFT)|INTEGER_BIT)};
struct String {
	char* s;
	int len;
	int size;
};

struct Register {
	long long val;
	struct ValType type;
};

// GLOBAL-VARIABLES
char* OPS[][8] = {{"(","[","{",""},{"*","/","%",""},{"+","-",""},{";",""},{""}};


int recurse(char*, int, struct Register, char**, struct Register*, int*, int level);


// TYPE-FUNCTIONS
short setErr(struct ValType* t) {
	t->type&=~TYPE_ERR_NAND;
	return t->type;
}
char isErr(struct ValType t) {
	return !(t.type&TYPE_ERR_NAND);
}
char isInt(struct ValType t) {
	return t.type&INTEGER_BIT;
}
char isSigned(struct ValType t) {
	return t.type&SIGNED_BIT;
}
int getSizeB(struct ValType t) {
	if (t.type&POINTER_BIT) return POINTER_SIZE;
	return t.type>>GET_SIZE_SHIFT;
}
int getSizeb(struct ValType t) {
	return getSizeB(t)*8;
}
char isChar(struct ValType t) {
	return t.type&CHARACTER_BIT;
}
int isPtr(struct ValType t) {
	if (t.type&POINTER_BIT) return t.type>>GET_SIZE_SHIFT;
	return NOT_POINTER;
}
char isVarContr(struct ValType t) {
	return t.type&VAR_CONTR_BIT;
}
// OP-FUNCTIONS
int isOp(char* cs) {
	int i = 0;
	for (; (cs[i]<'0'||cs[i]>'9')&&(cs[i]|32<'a'||cs[i]|32>'z')&&cs[i]!='_'&&cs[i]>32; i++);
	return i;
}
char isNum(char c) {
	return c>='0'&&c<='9'||c=='.';
}
int getOpPr(char* op, int len) {
	int i, j, k;
	for (i = 0, j, k; OPS[i][0][0]; i++) {
		for (j = 0; OPS[i][j][0]; j++) {
			for (k = 0; OPS[i][j][k]; k++) {
				if (OPS[i][j][k]!=op[k]) {
					break;
				}
			}
			if (k>=len-1&&!OPS[i][j][k]) return i;
		}
	}
	if (len==0) return i;
	return -1;
}
int doOp(char* op, int len, struct Register a, struct Register b, struct Register* o) {
	printf("\tOperation:");
	for (int i = 0; i<len; i++) {
		printf("%c", op[i]);
	}
	printf("\n");
	return 0;
}

int getNum(char* str, struct Register* ret) {
//	printf("getNum(...\n");
	int base = 10;
	long long n = 0;
	int topbase = 1, digits,
	    irad = -1,itype = 0,is=0,ie;
	struct ValType* t = &(ret->type);
	char rdBin = 0, rdHex = 0;
	for (digits = 0; str[digits]; digits++) {
		char C = str[digits], c = C|32;
		if ( c>='0'&&c<='9'|| c>='a'&&c<='f' );
		else if (c=='b') {
			rdBin = 1;
			is = digits+1;
		} else if (c=='x') {
			rdHex = 1;
			is = digits+1;
		} else if (c>='a'&&c<='z') {
			if (!itype) {
                                ie = digits;
                                itype = digits;
                        }
			switch (c) {
				case 'u':
					t->type&=~(UINT_NAND);
					t->type|=UINT_OR;
					break;
				case 'i':
					t->type&=~(SINT_NAND);
					t->type|=SINT_OR;
					break;
				case 'c':
					t->type&=~(CHAR_NAND);
					t->type|=CHAR_OR;
					break;
				case 'r':
//					printf("case 'r'\n");
					t->type&=~(FLOAT_NAND);
					t->type|=FLOAT_OR;
					break;
				case 'p':
					t->type|=POINTER_BIT;
					break;
			}
//			printf("getting size\n");
			t->type|=(short)((str[digits+1]-48)<<GET_SIZE_SHIFT);
		} else if (C=='.') {
			irad = digits;
		} else break;
	}
	if (isInt(*t)) {
		for (int i = ie-1; i>=is; i--)	{
			n+=(str[i]-48)*topbase;
			topbase*=base;
		}
	} else {
		double* dn = (double*)&n; *dn = 0.0;
		double dtopbase = 1;
//		printf("irad:%d, ie:%d\n", irad, ie);
		if (irad+1) {
			for (int i = irad+1; i<ie; i++) {
				dtopbase/=base;
				*dn+=(str[i]-48)*dtopbase;
//				printf("dn:%f, dtopbase:%f\n", *dn, dtopbase);
			}
		} else irad = ie;
		dtopbase = 1;
		for (int i = irad-1; i>=is; i--) {
			*dn+=(str[i]-48)*dtopbase;
			dtopbase*=base;
//			printf("dn=%f\n", *dn);
		}
//		printf("dn = %f;\n", *dn);
	}
	ret->val = n;
	return digits;
}
struct Register solve(char* expr) {
	struct Register ret;
	char* res_i = expr;
	int rOp_p = 0;
	if (isNum(*expr)) {
		expr+=getNum(expr, &ret);
		printf("number aquired\n");
	}
	recurse(expr, getOpPr("",0), ret, &res_i, &ret, &rOp_p, 0);
	return ret;
}
int recurse(char* expr, int pOp_p, struct Register pReg, char** res_i, struct Register* rReg, int* rOp_p, int level) {
	if (level>10) return -1;
	for (;*expr<33;expr++);
	struct Register cReg;
	int cOp_p = getOpPr(";",1), cOp_len = 0;
	char* cOp = expr;
	printf("I'm in.\n");
	if ((cOp_len=isOp(expr))) {
		cOp = expr;
		cOp_p = getOpPr(cOp, cOp_len);
		expr+=cOp_len;
		printf("operation aquired\n");
	}
	if (isNum(*expr)) {
		expr+=getNum(expr, &cReg);
		printf("number aquired\n");
	}
	printf("I'm... in.\npOp:%d, cOp:%d\n", pOp_p, cOp_p);
	if (pOp_p>cOp_p) {// continue
		recurse(expr, cOp_p, cReg, res_i, rReg, rOp_p, level+1);
		if (pOp_p>cOp_p) {// doop
			doOp(cOp, cOp_len, pReg, *rReg, rReg);
		}
		if (pOp_p<=*rOp_p) {// return or recurse
			return 0;
		} else {
			return recurse(*res_i, pOp_p, *rReg, res_i, rReg, rOp_p, level+1);
		}
	}
	*rOp_p = cOp_p;
	*res_i = cOp;
	return 0;
}

void printReg(struct Register* reg) {
	if (isInt(reg->type)) printf("%d",(int)reg->val);
	else printf("%f",*((double*)&reg->val));
}

int main(int argc, char** argv) {
	int i;
	for (i = 0; argv[1][i]; i++);
	struct Register reg = solve(argv[1]);
	printReg(&reg);
	printf("\nlength of string:%d\n", i);
	return 0;
}

