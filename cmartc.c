#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define INTEGER_BIT	1
#define SIGNED_BIT	2
#define CHARACTER_BIT	4
#define POINTER_BIT	8
#define VAR_CONTR_BIT	16
#define GET_SIZE_SHIFT	5
#define POINTER_SIZE	8
#define NOT_POINTER	-1

#define UINT_NAND	6
#define UINT_OR		1
#define SINT_NAND	4
#define SINT_OR		3
#define FLOAT_NAND	5
#define FLOAT_OR	2
#define CHAR_NAND	2
#define CHAR_OR		5
#define TYPE_ERR_NAND	3

// STRUCTS
struct ValType {
	short type;
};
struct String {
	char* s;
	int len;
	int size;
};
struct String bss;
struct String data;
struct String text;
struct Var {
	char* name;
	int rpos;
	struct ValType type;
};
struct VarList {
	struct Var** v;
	int len;
	int size;
};
struct VarList global;
struct VarList globfunc;
/*		{isInt(),	isSigned(),	getSizeB(),	isChar(),	isPtr()}
 * I4 ==	{true,		true,		4,		false,		-1}
 * I8 ==	{true,		true,		8,		false,		-1}
 * U2 ==	{true,		false,		2,		false,		-1}
 * F4 ==	{false,		true,		4,		false,		-1}
 * P0 ==	{false,		false,		8,		false,		0}
 * P8(F8) ==	{false,		true,		8,		false,		8}
 * P2(U2) ==	{true,		false,		8,		false,		2}
 * C1 ==	{true,		false,		1,		true,		-1}
 * B1 == U1
 */
struct Register {
	long long val;
	struct ValType type;
};
struct Fnc {
	struct String name;
	struct String text;
};

//TODO initialize global matrix for Operators and prioritization

// GLOBAL-VARIABLES
char* OPS[][8] = {{"(","[","{",""},{"*","/","%",""},{"+","-",""},{""}};

// DECLARE-FUNCTIONS
int itos(int,char*,int);
int stoi(char*);
int ltos(long long,char*,int);
long long stol(char*);
int ltoh(unsigned long long,char*);
char str_cmp(char*,char*,int);
int mn(int,int);
int mx(int,int);
short setErr(struct ValType*); // sets err bits and returns the type flags
char isErr(struct ValType); // returns true if the value has the err flags raised
char isInt(struct ValType); // returns true if the value is an integer and not a float
char isSigned(struct ValType); // returns true if the value is signed
int getSizeB(struct ValType); // returns size of value in register in Bytes
int getSizeb(struct ValType); // returns size of value in register in bits
char isChar(struct ValType); // returns true if the value is used as a character
int isPtr(struct ValType); // returns the size of the value pointed to
char isVarContr(struct ValType); // returns true if the value is a variable container
int isOp(char*); // input character start location, returns size of op
int getOpPr(char*, int); // input op returns priority of operand
long long enumOp(char*, int); // returns a long of the 8 byte operation padded with 0s.
int appOp(struct String*, char*, int, struct ValType); // input text string and the operand. appends propper asm instruction(s) for operation.
int grow(struct String*);
int init(struct String*,char*,int);
int del(struct String*);
int concat(struct String*, struct String*, struct String*);
int addc(struct String*,char*);
int cadd(char*,struct String*);
int initVL(struct VarList*, int);
int growVL(struct VarList*);
int addToVL(struct VarList*,struct Var*);
int delVar(struct Var*);
int initVar(struct Var*,char*,int);
struct Var* copyVar(struct Var*);
void normVar(struct Var*, struct Register*);
struct Var* searchVL(struct VarList*, char*, int);
int appLocVar(struct String*,struct Var*);
int appGloVar(struct String*,struct Var*);
int appRegister(struct String*,struct Register*);
void printReg(struct Register*);
long long getValue(struct VarList*, char*, struct Register*);
long long getNum(char*, struct Register*);
int recurse(struct VarList*, struct String*, char*, char**, int, struct Register);

// DEFINE-FUNCTIONS
int itos(int n, char* str, int base) {
	int topbase = 1, digits;
	for (topbase = 1; n/topbase>base; topbase*=base);
	for (digits = 0; topbase>0;topbase/=base) {
		str[digits] = n/topbase+48;
		n-=n/topbase*topbase;
		digits++;
	}
	return digits;
}
int stoi(char* str) {
	int base = 10;
	int topbase, digits, n;
	for (topbase = 1, digits = 0; str[digits]>='0'&&str[digits]<='9'; topbase*=base) digits++;
	for (int i = 0; i<digits; i++)	{
		topbase/=base;
		n+=(str[i]-48)*topbase;
	}
	return n;
}
int ltos(long long n, char* str, int base) {
	long long topbase = 1;
	int digits;
	for (topbase = 1; n/topbase>base; topbase*=base);
	for (digits = 0; topbase>0;topbase/=base) {
		str[digits] = n/topbase+48;
		n-=n/topbase*topbase;
		digits++;
	}
	return digits;
}
long long stol(char* str) {
	int base = 10;
	long long topbase, n;
	int digits;
	for (topbase = 1, digits = 0; str[digits]>='0'&&str[digits]<='9'; topbase*=base) digits++;
	for (int i = 0; i<digits; i++)	{
		topbase/=base;
		n+=(str[i]-48)*topbase;
	}
	return n;
}
int ltoh(unsigned long long n, char* str) {
	str+=16;
	for (int i = 0; i<16; i++) {
		char c = (n&0xf)+48;
		n>>=4;
		if (c>57) c+=7;
		*(--str) = c;
	}
	return 16;
}
char str_cmp(char* template, char* data, int dlen) {
	for (int i = 0; i<dlen; i++) {
		if (*template!=*data) return 0;
		++template;++data;
	}
	return !*(data+1);
}
int mn(int a, int b) {
	return (a>b)? b: a;
}
int mx(int a, int b) {
	return (a>b)? a: b;
}
// TYPE-FUNCTIONS
short setErr(struct ValType* t) {
	t->type&=~TYPE_ERR_NAND;
	return t->type;
}
char isErr(struct ValType t) {
	return !(t.type&TYPE_ERR_NAND);
}
char isInt(struct ValType t) {
	return t.type&INTEGER_BIT;
}
char isSigned(struct ValType t) {
	return t.type&SIGNED_BIT;
}
int getSizeB(struct ValType t) {
	if (t.type&POINTER_BIT) return POINTER_SIZE;
	return t.type>>GET_SIZE_SHIFT;
}
int getSizeb(struct ValType t) {
	return getSizeB(t)*8;
}
char isChar(struct ValType t) {
	return t.type&CHARACTER_BIT;
}
int isPtr(struct ValType t) {
	if (t.type&POINTER_BIT) return t.type>>GET_SIZE_SHIFT;
	return NOT_POINTER;
}
char isVarContr(struct ValType t) {
	return t.type&VAR_CONTR_BIT;
}
// OP-FUNCTIONS
int isOp(char* cs) {
	int i = 0;
	for (; (cs[i]<'0'||cs[i]>'9')&&(cs[i]|32<'a'||cs[i]|32>'z')&&cs[i]!='_'&&cs[i]>32; i++);
	return i;
}
int getOpPr(char* op, int len) {
	for (int i = 0, j, k; OPS[i][0][0]; i++) {
		for (j = 0; OPS[i][j][0]; j++) {
			for (k = 0; OPS[i][j][k]; k++) {
				if (OPS[i][j][k]!=op[len]) {
					break;
				}
			}
			if (k>=len-1&&!OPS[i][j][k]) return i;
		}
	}
	return -1;

}
long long enumOp(char* op, int len) {
	long long iop = 0;
	for (int i = 0; i<len; i++)
		*(((char*)&iop)+i) = *(op+i);
	return iop;
}
int appOp(struct String* text, char* op, int len,  struct ValType type) {
	long long e = enumOp(op, len);
	char args[] = " ax,  bx\n";
	int sizeB = getSizeB(type);
	switch (sizeB) {
		case 8:
			args[0] = 'r';
			args[5] = 'r';
			break;
		case 4:
			args[0] = 'e';
			args[5] = 'e';
			break;
		case 2:
			break;
		case 1:
			args[2] = 'l';
			args[7] = 'l';
			break;
		default:
			args[0] = 'r';
			args[5] = 'r';
			break;
	}
	switch (e) {
		case 0x2a: // "*"
			addc(text, "mul "); addc(text,args);
			break;
		case 0x2f: // "/"
			addc(text, "div "); addc(text,args);
			break;
		case 0x25: // "%"
			break;
		case 0x2b: // "+"
			addc(text,"add "); addc(text,args);
			break;
		case 0x2d: // "-"
			addc(text,"sub "); addc(text,args);
			break;
		case 0x2b2b: // "++"
			break;
		case 0x2d2d: // "--"
			break;
		case 0x21: // "!"
			break;
		case 0x2626: // "&&"
			break;
		case 0x7c7c: // "||"
			break;
		case 0x7e: // "~"
			break;
		case 0x26: // "&"
			break;
		case 0x7c: // "|"
			break;
		case 0x5e: // "^"
			break;
		case 0x3c: // "<"
			break;
		case 0x3c3d: // "<="
			break;
		case 0x3d3d: // "=="
			break;
		case 0x3d: // "="
			break;
		case 0x3e3d: // ">="
			break;
		case 0x3e: // ">"
			break;
		case 0x3f: // "?"
			break;
		case 0x3a: // ":"
			break;
		case 0x2e: // "."
			break;
		case 0x2c: // ","
			break;
		default:
			break;
	}
	return 0;
}
	
// TODO implement ops
// STRING-FUNCTIONS
int grow(struct String* str) {
	int nsize = str->size*2;
	char* ns = calloc(nsize, sizeof(char));
	for (int i = 0; i<str->len; i++) {
		ns[i] = str->s[i];
	}
	free(str->s);
	return nsize;
}

/*
int init(struct String* str) {
	str->size = 64;
	str->s = calloc(str->size, sizeof(char));
	return str->size;
}
int init(struct String* str, int size) {
	str->size = size;
	str->s = calloc(str->size, sizeof(char));
	return str->size;
}
*/
int init(struct String* str, char* c, int size) {
	int clen = 0;
	if (c==NULL) {
		str->size = 1<<((sizeof(int)<<3)-__builtin_clz(size));
		str->s = calloc(str->size, sizeof(char));	
	} else {
		for (int i = 0; c[i]; i++) {
			clen++;
		}
		if (size<=clen) size = clen+1;
		str->len = clen;
		str->size = 1<<((sizeof(int)<<3)-__builtin_clz(size));
		str->s = calloc(str->size, sizeof(char));
		for (int i = 0; c[i]; i++) {
			str->s[i] = c[i];
		}
	}
	return str->len;
}
int del(struct String* str) {
	free(str->s);
	str->s = 0;
}
int concat(struct String* a, struct String* b, struct String* result) {
	int max = mx(a->size, b->size);
	result->len = a->len+b->len;
	if (result->len>=max) {
		max*=2;
	}
	init(result, NULL,  max);
	for (int i = 0; a->s[i]; i++) {
		result->s[i] = a->s[i];
	}
	for (int i = 0; b->s[i]; i++) {
		result->s[i+a->len] = b->s[i];
	}
	return max;
}
/*
int add(struct String* str, char c) {
	str->s[str->len] = c;
	str->len++;
	if (str->len+1<=str->size) {
		grow(str);
	}
	return str->len;
}
*/
int addc(struct String* str, char* c) {
	int clen = 0;
	for (int i = 0; c[i]; i++) {
		clen++;
	}
	struct String b = {.s = c, .len = clen, .size = 1<<((sizeof(int)<<3)-__builtin_clz(clen))}, res;
	concat(str, &b, &res);
	del(str);
	str->s = res.s;
	str->len = res.len;
	str->size = res.size;
	return str->len;
}
int cadd(char* c, struct String* str) {
	int clen = 0;
	for (int i = 0; c[i]; i++) {
		clen++;
	}
	struct String b = {.s = c, .len = clen, .size = 1<<((sizeof(int)<<3)-__builtin_clz(clen))}, res;
	concat(&b, str, &res);
	del(str);
	str->s = res.s;
	str->len = res.len;
	str->size = res.size;
	return str->len;
}
int initVL(struct VarList* vl, int size) {
	vl->size = size;
	vl->v = calloc(size, sizeof(void*));
	vl->len = 0;
	return vl->size;
}
int growVL(struct VarList* vl) {
	int newsize = vl->size*2;
	struct Var** nl = calloc(newsize, sizeof(void*));
	for (int i = 0; i<vl->len; i++) {
		nl[i] = vl->v[i];
	}
	free(vl->v);
	vl->v = nl;
	vl->size = newsize;
	return vl->size;
}
int addToVL(struct VarList* vl, struct Var* v) {
	vl->v[vl->len] = copyVar(v);
	vl->len++;
	if (vl->len>vl->size/2) {
		growVL(vl);
	}
	return vl->len;
}
int delVar(struct Var* v) {
	free(v->name);
}
int initVar(struct Var* v, char* name, int rpos) {
	int nlen = 1;
	for (int i = 0; name[i]; i++) {
		nlen++;
	}
	v->name = calloc(nlen, sizeof(char));
	for (int i = 0; i<nlen; i++) {
		v->name[i] = name[i];
	}
	v->rpos = rpos;
	return v->rpos;
}
/**
 * returns a pointer to a copy of the Var structure
 */
struct Var* copyVar(struct Var* v) {
	struct Var* cpv = calloc(1, sizeof(struct Var));
	initVar(cpv, v->name, v->rpos);
	cpv->type = v->type;
	return cpv;
}
/**
 * normalizes a Var structure as a Register
 */
void normVar(struct Var* v, struct Register* r) {
	r->type = v->type;
	r->type.type&=VAR_CONTR_BIT;
	r->val = v->rpos;
}
/**
 * searches the VarList provided for (char*) name
 */
struct Var* searchVL(struct VarList* vl, char* name, int name_len) {
	for (int i = 0; i<vl->len; i++) {
		if (str_cmp(vl->v[i]->name, name, name_len)) {
			return vl->v[i];
		}
	}
	return NULL;
}
/**
 * appends the variable in assembly format
 */
int appLocVar(struct String* str, struct Var* v) {
	char* varRep = calloc(16, sizeof(char));
	varRep[0] = '[';
	varRep[1] = 's';
	varRep[2] = 'p';
	varRep[3] = 'i';
	varRep[4] = ' ';
	varRep[5] = '+';
	varRep[6] = ' ';
	itos(v->rpos, varRep+7, 10);
	int i;
	for (i = 0; varRep[i]; i++);
	varRep[i] = ']';
//	varRep[i+1] = '\n';
	addc(str, varRep);
	free(varRep);
	return str->len;
}
int appGloVar(struct String* str, struct Var* v) {// TODO
	char* varRep = calloc(16, sizeof(char));
	varRep[0] = '[';
	varRep[1] = 's';
	varRep[2] = 'p';
	varRep[3] = 'i';
	varRep[4] = ' ';
	varRep[5] = '+';
	varRep[6] = ' ';
	itos(v->rpos, varRep+7, 10);
	int i;
	for (i = 0; varRep[i]; i++);
	varRep[i] = ']';
//	varRep[i+1] = '\n';
	addc(str, varRep);
	free(varRep);
	return str->len;
}

/**
 * appends the Register as a local Variable or as a numerical value
 */
int appRegister(struct String* str, struct Register* r) {
	if (isVarContr(r->type)) {
		if (r->val<0) {
			switch (r->val) {
				case -2:
					addc(str, "rax");
					break;
				case -3:
					addc(str, "rbx");
					break;
				case -4:
					addc(str, "rcx");
					break;
				default:
					addc(str, "; error in appRegister(), r!=[-2,-4] && r<0;");
					break;
			}
			return str->len;
		}
		struct Var tempvar = {.name=0,.rpos=(int)r->val};
		return appLocVar(str, &tempvar);
	}
	char* varRep = calloc(10, sizeof(short));
	ltoh(r->val, varRep);
	int i = 0;
	for (; varRep[i]; i++);
	varRep[i] = 'h';
//	varRep[i+1] = '\n';
	addc(str, varRep);
	free(varRep);
	return str->len;
}

/**
 * prints the Register as a float or int depending on the ValType in the Register
 */
void printReg(struct Register* reg) {
	if (isInt(reg->type)) printf("%d",(int)reg->val);
	else printf("%f",*((double*)&reg->val));
}

/**
 * either retrieves the next variable or calls getNum depending on the first character
 * return value is stored in register either as a register value or a variable container
 */
long long getValue(struct VarList* local, char* str, struct Register* ret) {
	for (;*str<33;str++);
	if (*str>='0'&&*str<='9'||*str=='.') {
		return getNum(str,ret);
	} else {
		int i;
		for (i = 0; str[i]|32<='z'&&str[i]|32>='a'||str[i]>='0'&&str[i]<='9'||str[i]=='_'; i++);
		struct Var* v = searchVL(local, str, i);
		if (v==NULL) return -1;
		normVar(v,ret);
		return ret->val;
	}
}

/**
 * retrieves the next number in std-cmart-fmt and returns it as the Register* (ret)
 */
long long getNum(char* str, struct Register* ret) {
//	printf("getNum(...\n");
	int base = 10;
	long long n = 0;
	int topbase = 1, digits,
	    irad = -1,itype = 0,is=0,ie;
	struct ValType* t = &(ret->type);
	char rdBin = 0, rdHex = 0;
	for (digits = 0; str[digits]; digits++) {
		char C = str[digits], c = C|32;
		if ( c>='0'&&c<='9'|| c>='a'&&c<='f' );
		else if (c=='b') {
			rdBin = 1;
			is = digits+1;
		} else if (c=='x') {
			rdHex = 1;
			is = digits+1;
		} else if (c>='a'&&c<='z') {
			if (!itype) {
				ie = digits;
				itype = digits;
			}
			switch (c) {
				case 'u':
					t->type&=~(UINT_NAND);
					t->type|=UINT_OR;
					break;
				case 'i':
					t->type&=~(SINT_NAND);
					t->type|=SINT_OR;
					break;
				case 'c':
					t->type&=~(CHAR_NAND);
					t->type|=CHAR_OR;
					break;
				case 'r':
//					printf("case 'r'\n");
					t->type&=~(FLOAT_NAND);
					t->type|=FLOAT_OR;
					break;
				case 'p':
					t->type|=POINTER_BIT;
					break;
			}
//			printf("getting size\n");
			t->type|=(short)((str[digits+1]-48)<<GET_SIZE_SHIFT);
		} else if (C=='.') {
			irad = digits;
		} else break;
	}
	if (isInt(*t)) {
		for (int i = ie-1; i>=is; i--)	{
			n+=(str[i]-48)*topbase;
			topbase*=base;
		}
	} else {
		double* dn = (double*)&n; *dn = 0.0;
		double dtopbase = 1;
		if (irad+1) {
			for (int i = irad+1; i<ie; i++) {
				dtopbase/=base;
				*dn+=(str[i]-48)*dtopbase;
//				printf("dn:%f\n", *dn);
			}
		} else irad = ie;
		dtopbase = 1;
		for (int i = irad-1; i>=is; i--) {
			*dn+=(str[i]-48)*dtopbase;
			dtopbase*=base;
//			printf("dn=%f\n", *dn);
		}
//		printf("dn = %f;\n", *dn);
	}
	ret->val = n;
	return n;
}
/**
 * return values:
 * 	0 [==]: move fwd
 * 	1 [<]:  move bck, until beginning then move fwd
 * 	  [>]:  continue
 * 	2 [END]: move bck, then end
*/
int recurse(struct VarList* local, struct String* text, char* expr, char** reti, int lp, struct Register lv) {
	//TODO
	int tp, tol; char* top;
	struct Register tv;
	for (int i = 0; expr[i]; i++) {
		if ((tol = isOp(expr+i))) {
			top = expr+i;
			tp = getOpPr(top, tol);
			i+=tol;
			long long valret = getValue(local, top+tol, &tv);
			if (lp>tp) {
				int ret = recurse(local, text, top, reti, tp, tv);
				addc(text, "mov rbx, rax\nmov rax, ");
				appRegister(text, &lv);
				appOp(text, top, tol, lv.type);
				if (!ret) { // returned [==] doop and move fwd
					return recurse(local, text, top, reti, lp, 
				} else { // returned [<] or [END] doop and move bck
					
				}
			} else if (lp<tp) {
				*reti = top;
				
			} else {
			}
		}
	}
	return 0;
}

// MAIN
int main(int argc, char** argv) {
	init(&text, "", 32);
	char input[256] = {0};
	char output[256] = {0};
	read(0, input, 256);
	struct Register reg = {.val = 0, .type = 0};
	getNum(input,&reg);
	printReg(&reg);
	printf(", of type: %x\n", reg.type.type);
	ltoh(reg.val, output);
	printf("ltoh() = %s\n", output);
	printf("%s", text.s);
	del(&text);
	return 0;
}

