#include <stdio.h>


int main(int argc, char** argv) {
	int i = 34;
//	printf("clz(%d) = %d", i, sizeof(int)-__builtin_clz(i));
	printf("%d -> %d", i, 1<<((sizeof(int)<<3)-__builtin_clz(i)));
	return 0;
}
